const
	path = require('path'),
	express = require('express'),
	favicon = require('serve-favicon'),
	open = require('open'),
	webpack = require('webpack'),
	webpack_config = require('../../webpack.config.dev'),
	_ = require('underscore'),

	app = express(),
	port = 3000,
	compiler = webpack(webpack_config);

app.use(favicon(path.join(__dirname,'../../src/assets/favicon.png')));
app.use(require('webpack-dev-middleware')(compiler,{
	publicPath: webpack_config.output.publicPath
}));

var users = [
			{'id':1,'fName':'Mykola', 'lName':'Ilkiv', 'dob':'16/12/1993'},
			{'id':2,'fName':'Luke', 'lName':'Lucky', 'dob':'16/12/1994'},
			{'id':3,'fName':'Luke', 'lName':'Unlucky', 'dob':'16/12/1995'}
		]; 
app.get('/user/list', function (req,res){
	res.send(
		[
			{'id':1,'fName':'Mykola', 'lName':'Ilkiv', 'dob':'16/12/1993'},
			{'id':2,'fName':'Luke', 'lName':'Lucky', 'dob':'16/12/1994'},
			{'id':3,'fName':'Luke', 'lName':'Unlucky', 'dob':'16/12/1995'}
		]
	);
});
app.get('/userinfo/:userId', function (req,res){
	debugger;
	var toRet = _.findWhere(users, {id: parseInt(req.params.userId)});
	res.send(toRet);
});
app.get('/*',(req,res) => res.sendFile(path.join(__dirname,'../../src/index.html')));


app.listen(port, err => err ? console.log(err) : open(`http://localhost:${port}`));