
RootCtrl.$inject = ['postsService','userInfo', '$q']; 
function RootCtrl (postsService,userInfo, $q) {
	var self = this;

	console.log("Main componenet is alive");
	//getting array of posts by using service
	self.posts = postsService.getPosts(1,10);
};



angular.module('app').component('root', {
	template: `
		<div class="container">
		    <div class="content">
		        <post-block data="post" ng-repeat="post in $ctrl.posts track by $index">

		        </post-block>
		    </div>
		    <aside-menu class="aside"><aside-menu>
		</div>
		`,
	controller: RootCtrl
});