angular.module('app')
.controller("HeaderCtrl", HeaderCtrl);

//injecting userInfo service
HeaderCtrl.$inject = ['userInfo'];
function HeaderCtrl(userInfo) {
	var self = this;

	//async call for data to server. 
 	userInfo.getUserById().then(function (user){
 		self.user = user;
 		console.log("HeaderCtrl, userInfo.getUserById promise fired.");
 		console.log(self.user);
 	}); 
	console.log('Navbar controller is alive');
} 


angular.module('app').component('navbar', {
	template: `<header class="page-header">
		    <!--navbar block-->
		    <div class="navbar">
		        <a class="logo" href="#" onclick="callFunc()"></a>
		        <ul class="nav-h">
		            <li>
		                <a class="btn profile profile--hover-bg" href="/details/{{$ctrl.user.id}}">
		                    <img src="./../assets/profile.gif">
		                    <span>{{$ctrl.user.fName}} {{$ctrl.user.lName}}</span>
		                </a>
		            </li>
		            <li>
		                <a class="btn" href="/">
		                    <span>Home</span>
		                </a>
		            </li>
		        </ul>
		    </div>
		</header>`,
	controller: HeaderCtrl,
});
