angular.module('app')
.component('userDetails', {
	template: 	
		`<div class="container">
		    <div class="content">
		    	<br>
				<div class="horizontal-flex">
					<div>
						<img width="100px" height="100px" src="http://www.imagespourtoi.com/lesimages/luky-luke/image-luky-luke-2.gif"/>
					</div>
					<div>
						<div ng-show="$ctrl.user"> 
							<div>
								<strong>Id: </strong> {{$ctrl.user.id}}
							</div>
							<div>
								<strong>First Name: </strong> {{$ctrl.user.fName}}
							</div>
							<div>
								<strong>Last Name: </strong> {{$ctrl.user.lName}}
							</div>
							<div>
								<strong>Date of birth: </strong> {{$ctrl.user.dob}}
							</div>
						</div>
						<div ng-hide="$ctrl.user"> 
							No user was found for reffered id.
						</div>
					</div>
				</div>
			</div>
		    <aside-menu class="aside"><aside-menu>
		</div>`,
	controller: ['userInfo', '$stateParams', function (userInfo,$stateParams) {
		console.log('userDetails component is alive');
		var self = this;
		

		userInfo.getUserById($stateParams.userId).then(function (askedUser) {
			self.user = askedUser;
		});
	}]
});