AddCommentCtrl.$inject = ['userInfo', 'nofilterFilter']; 
function AddCommentCtrl (userInfo, nofilterFilter) {
    var self = this;
    self.comment = "";

    //load current user info
    userInfo.getUserById().then(function (user) {
        self.currentUser = user;
    });

    console.log('Post-block component is alive');


    //add comment to the post by using one of the services.
    self.addComment = function () {
        if (!self.currentUser) {
            alert('Loading information for current user failed.')
        }
    	if (self.comment && self.comment.trim()) {
            var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
            var currTime = new Date();
            self.comment = nofilterFilter(self.comment);
    		self.comments.push({
               "username":self.currentUser.fName+' '+self.currentUser.lName,
               "commentBody":self.comment,
               "date": monthNames[currTime.getMonth()] + ' ' + currTime.getDate() + ' at ' + currTime.getHours()+':'+currTime.getMinutes()
            });
    	} 
    	self.comment = "";
    };


}

angular.module('app')
.component('addComment', {
    bindings: {
        comments: '='
    },
    template:  
        `<textarea ctrl-save="$ctrl.addComment()" ng-model="$ctrl.comment" class="p-input__ta ng-pristine ng-valid ng-binding ng-empty ng-touched" data-gramm="true" data-txt_gramm_id="7fc99c67-9db7-160e-ec45-4d3ee7b55125" data-gramm_id="7fc99c67-9db7-160e-ec45-4d3ee7b55125" spellcheck="false" data-gramm_editor="true"></textarea>
        <small><small>Your actual comment ('no' will be replaced by 'YES'): {{$ctrl.comment | nofilter}}</small></small>
        <a class="btn btn-primary f-right margin-8-v" href="#" ng-click="$ctrl.addComment()">POST</a>`,
    controller: AddCommentCtrl
});