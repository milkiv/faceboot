
CommentsCtrl.$inject = []; 
function CommentsCtrl () {
    console.log('Comments holder component is alive');
    var self = this; 
}


angular.module('app')
.component('comments', {
    bindings: {
        comment: '='
    },
    template:  
        `<li class="flex-col">
            <!--USER COMMENT HEADER-->
            <div class="flex-between">
                <a class="profile profile--hover-ul" href="#">
                    <img src="./assets/stock_profile.jpg">
                    <span>{{$ctrl.comment.username}}</span>
                </a>
                <span class="timestamp text-sm">{{$ctrl.comment.date}}</span>
            </div>
            <!--USER COMMENT CONTENT-->
            <p class="text-md margin-8-top">
                {{$ctrl.comment.commentBody}}
                
            </p>
        </li>`,
    controller: CommentsCtrl
});