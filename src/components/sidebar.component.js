angular.module('app')
.component('asideMenu', {
	template: 	
		`<div class="aside">
			<a class="link" ng-repeat="user in $ctrl.users" href="/details/{{user.id}}"><div>{{user.lName}} {{user.fName}}</div></a>
        </div>`,
	controller: ['userInfo', function (userInfo) {
		console.log('Sidebar is alive');
		var self = this;
		userInfo.getUsers().then(function (response) {
			self.users = response;
		});
	}]
});
