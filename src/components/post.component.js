
PostCtrl.$inject = []; 
function PostCtrl () {
    this.username = "Mykola Ilkiv";
    console.log('Post-block component is alive');
    var self = this; 

    self.likeUnlike = function () {
        self.liked ? self.data.likesCount++ : self.data.likesCount--;
    }
}

angular.module('app')
.component('postBlock', {
    bindings: {
        data: '=',
        currentUser: '='
    },
    template:   
        `<!-- USER POST-->
        <div class="post">
            <!--USER POST HEADER-->
            <div class="post__header flex-between margin-16">
                <a class="profile profile--lg profile--hover-ul" href="#">
                    <img src="./assets/stock_profile.jpg">
                    <span>{{$ctrl.data.author}}</span>
                </a>
                <span class="timestamp">{{$ctrl.data.date}}</span>
            </div>
            <!--USER POST CONTENT-->
            <div class="text-md margin-16">
                {{$ctrl.data.postBody}}
            </div>
            <!--USER POST FOOTER-->
            <div class="flex-between margin-16">
                <div class="flex">
                    <!-- TODO: ask Keegan what is better to do? send event dom object to ng-click function and set class inside the function (looks like not the best Angular practice) or as it is done. -->
                    <a ng-click="$ctrl.liked = !$ctrl.liked; $ctrl.likeUnlike()" ng-class="{'btn--active' : $ctrl.liked}" class="btn-icon btn--hover-red" href="#"><i class="fa fa-heart fa-lg"></i></a>
                    <span>{{$ctrl.data.likesCount}}</span>
                </div>
                <div class="flex">
                    <a  class="btn-icon" href="#"><i class="fa fa-comments-o fa-lg"></i></a>
                    <span>{{$ctrl.data.comments.length}}</span>
                </div>
            </div>
            <!--USER POST COMMENTS-->
            <ul class="margin-16 top-divider">
                <div class="comments">
                    <!--USER COMMENT-->
                    <comments comment="comment" ng-repeat="comment in $ctrl.data.comments"></comments>
                </div>
                <!-- POST INPUT COMPONENT-->
                <div class="p-input">
                    <add-comment comments="$ctrl.data.comments"><add-comment>
                </div>
            </ul>
        </div>`,
    controller: PostCtrl
});