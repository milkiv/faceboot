require('./assets/styles/index.css');
require('angular');
require('angular-ui-router');
// require('Font-Awesome');


angular.module('app',['ui.router']);
angular.module('app').config(function ($stateProvider, $locationProvider, $urlRouterProvider) {
	$locationProvider.html5Mode(true); 
	$urlRouterProvider.otherwise('/');

	// routing by using $stateProvider
	$stateProvider.state("root", {
		url: '/', 
		component: 'root'
	}).state("userDetails", {
		url: '/details/:userId', 
		component: 'userDetails'
	});
});
angular.module('app').run(function () {
	console.log("Module is alive.");
});


angular.module('app').service('userInfo', ['$filter','$http',function ($filter, $http){
	// var userList = [
	// 	{'id':1,'fName':'Mykola', 'lName':'Ilkiv', 'dob':'16/12/1993'},
	// 	{'id':2,'fName':'Luke', 'lName':'Lucky', 'dob':'16/12/1994'},
	// 	{'id':3,'fName':'Luke', 'lName':'Unlucky', 'dob':'16/12/1995'}
	// ];

	var userList = [];

	this.getUsername = function () {
		return 'Mykola Ilkiv';
	}

	this.getDOB = function () {
		return '16/12/1993';
	}

	this.getCurrentUserInfo = function () {
		return userList && userList.length > 0 ? userList[0] : null;
	}

	this.getUserInfoById = function (userId) {
		var user = $filter('filter')(userList, {id: userId});
		return user && user.length > 0 ? user[0] : null;
	}

	this.getUserById = function (userId) {
		if (!userId) {
			userId = 1;
		}
		return $http({
			method: 'GET',
			url: '/userinfo/'+userId
		}).then(function success(response) {
			console.log('Server request getUserById was successful.');
			return response.data;
		}, function error(reason) {
			console.log('Server request getUserById was failed.');
			console.log('Reason:');
			console.log(reason);
			return null;
		});
	}

	this.getUsers = function getUsers () {
		return $http({
			method: 'GET',
			url: '/user/list'
		}).then(function successCallback(response) {
			console.log('Successful request.');
			userList = response.data;
			return userList;
		}, function errorCallback (reason) {
			console.log('Request failed.');
			userList = [];
			return userList;
		});
	};

	this.getUsers();
}]);

//directive for storing posts' data
angular.module('app').service('postsService', function () {
	var postsData = [
		{
			"author":"Lucky Luke", 
			"date":"May 31 at 9:59pm",
			"postBody":"is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", 
			"likesCount":"35", 
			"commentsCount":"20",
			"comments": [
				{
					"username":"Unlucky Luke",
					"commentBody":"It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
					"date":"May 31 at 9:59pm"			
				},
				{
					"username":"Lucky Luke",
					"commentBody":"Cool response here.",
					"date":"May 31 at 9:59pm"			
				}
			]},
			{
			"author":"Lucky Luke", 
			"date":"May 31 at 9:59pm",
			"postBody":"is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", 
			"likesCount":"35", 
			"commentsCount":"20",
			"comments": [
				{
					"username":"Unlucky Luke",
					"commentBody":"It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
					"date":"May 31 at 9:59pm"			
				},
				{
					"username":"Lucky Luke",
					"commentBody":"Cool response here.",
					"date":"May 31 at 9:59pm"			
				}
			]}
	];
	this.getPosts = function (page, size) {
		if (page<1 || size<1) {
			console.log('Invalid size or page number.');
			return [];
		}
		if (postsData && postsData.length>(page-1)*size) {
			return postsData.slice((page-1)*size,size);
		} else {
			console.log('Invalid format of posts data.');
			return [];
		}
	};
})

//filter which is replacing all 'no' words in the target string by word 'yes'
angular.module('app').filter('nofilter', function() {
  	return function(input) {

  		var out = input.replace(/no/gi, "YES");
		console.log(out);

    	return out;
  	};
});

//directive which will call appropriate function (reffered to the directive) when Ctrl+S will be fired.
angular.module('app').directive('ctrlSave', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
        	console.log('Keypress event fired');
            if(event.ctrlKey  && event.which === 83) {
                scope.$apply(function (){
                    scope.$eval(attrs.ctrlSave);
                });
                console.log('Ctrl-s pressed');
                event.preventDefault();
            }
        });
    };
});

const context = require.context('.',true,/\.component\.js$/);
context.keys().forEach(context);

console.log(angular);

angular.module('app').service 