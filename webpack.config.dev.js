// const path = require('path');

const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require('path');
const root = path.resolve(__dirname, 'src');

module.exports = {
	devtool: 'inline-source-map',
	entry: [
		path.resolve(__dirname, 'src/app.js')
	],
	target: 'web',
	output: {
		path: path.resolve(__dirname, 'src'),
		publicPath: '/',
		filename: 'bundle.js'
	},
	plugins: [new CopyWebpackPlugin([
             {
                 from: path.join(root, 'assets'),
                 to: 'assets',
                 ignore: ['*.css'],
             }
         ])],
	module: {
		rules: [
			{test: /\.css$/, loaders: ['style-loader','css-loader']}
		]
	}
}